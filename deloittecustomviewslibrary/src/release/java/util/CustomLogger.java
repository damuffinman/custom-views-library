package util;

import android.util.Log;

/**
 * Custom Log class for logcat statements. Only logs if forceLogging is set to true.
 */
public final class CustomLogger {
    private boolean mLogInReleaseVariant = false;
    private String mClassName;

    public CustomLogger(boolean logInReleaseVariant, String className) {
        mLogInReleaseVariant = logInReleaseVariant;
        mClassName = className;
    }

    public void d(String msg) {
        if (mLogInReleaseVariant) {
            Log.d(mClassName, msg);
        }
    }

    public void w(String msg) {
        if (mLogInReleaseVariant) {
            Log.w(mClassName, msg);
        }
    }

    public void w(String msg, Throwable tr) {
        if (mLogInReleaseVariant) {
            Log.w(mClassName, msg, tr);
        }
    }

    public void i(String msg) {
        if (mLogInReleaseVariant) {
            Log.i(mClassName, msg);
        }
    }

    public void e(String msg) {
        if (mLogInReleaseVariant) {
            Log.e(mClassName, msg);
        }
    }

    public void e(String msg, Throwable tr) {
        if (mLogInReleaseVariant) {
            Log.e(mClassName, msg, tr);
        }
    }
}
