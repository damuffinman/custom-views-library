package animatedrecyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.uk.deloitte.digital.customviewslib.R;

/**
 * Custom Adapter for the AnimatedRecyclerView
 */
public abstract class AnimatedRecyclerViewAdapter<VH extends AnimatedRecyclerViewHolder> extends RecyclerView.Adapter<VH> {
    protected Context mContext;
    private int lastPosition = -1;

    private static final int SLIDE_IN_FROM_BOTTOM_MAX_DELAY = 750;
    private static final int SLIDE_IN_FROM_LEFT_MAX_DELAY = 200;

    protected AnimatedRecyclerViewAdapter(Context context) {
        mContext = context;
    }

    /**
     * Animates the binding of the viewholder to the recyclerview - the item will fly up from
     * the bottom margin of the list. Call this from the onBindViewHolder method.
     * @param holder
     * @param position
     */
    public void slideViewFromBottomOnBind(VH holder, int position) {
        animate(holder, position, R.anim.slide_in_bottom, SLIDE_IN_FROM_BOTTOM_MAX_DELAY);
    }

    /**
     * Animates the binding of the viewholder to the recyclerview - the item will fly in from
     * the left margin of the list. Call this from the onBindViewHolder method.
     * @param holder
     * @param position
     */
    public void slideViewFromLeftOnBind(VH holder, int position) {
        animate(holder, position, R.anim.slide_in_left, SLIDE_IN_FROM_LEFT_MAX_DELAY);
    }

    /**
     * Animates the binding of the viewholder to the recyclerview - the item will fade in slowly.
     * Call this from the onBindViewHolder method.
     * @param holder
     * @param position
     */
    public void alphaInViewOnBind(VH holder, int position) {
        animate(holder, position, R.anim.fade_in, SLIDE_IN_FROM_LEFT_MAX_DELAY);
    }

    private void animate(VH holder, int position, int animationFile, long maxDelay) {
        if (holder.rootView == null) {
            return;
        }

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, animationFile);
            animation.setStartOffset(Math.min(50 * position, maxDelay));
            holder.rootView.startAnimation(animation);
            lastPosition = position;
        }
    }
}
