package animatedrecyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Custom ViewHolder for the Animated RecyclerView.
 *
 * Extend this class. You must pass the root container for the viewholder when instantiating this class.
 *
 */
public abstract class AnimatedRecyclerViewHolder extends RecyclerView.ViewHolder {
    protected View rootView;

    public AnimatedRecyclerViewHolder(View view, View rootView) {
        super(view);
        this.rootView = rootView;
    }
}
