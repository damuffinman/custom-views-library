package animatedrecyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.uk.deloitte.digital.customviewslib.R;

public class RecyclerViewFastScroller extends FrameLayout {

    private int mTotalHeight;
    private ImageView mScroller;
    private ImageButton mScrollbarHandle;
    private RecyclerView mRecyclerView;
    private ScrollListener mScrollListener;

    private boolean mIsHandleEnabled;
    private boolean mIsScrollbarEnabled;

    public RecyclerViewFastScroller(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup(context);
    }

    public RecyclerViewFastScroller(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup(context);
    }

    private void setup(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.fastscroll, this);
        mScroller = (ImageView) view.findViewById(R.id.scroller);
        mScrollbarHandle = (ImageButton) view.findViewById(R.id.scroller_handle);

        mIsHandleEnabled = true;
        mIsScrollbarEnabled = true;

        mScrollListener = new ScrollListener();
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        mRecyclerView = recyclerView;
        mRecyclerView.addOnScrollListener(mScrollListener);
    }

    /**
     * Enable/disable the custom scrollbar
     * @param setEnabled
     */
    public void setScrollbarEnabled(boolean setEnabled) {
        mIsScrollbarEnabled = setEnabled;
    }

    /**
     * Enable/disable the custom scrollbar handle
     * @param setEnabled
     */
    public void setScrollbarHandleEnabled(boolean setEnabled) {
        mIsHandleEnabled = setEnabled;
    }

    /**
     * Replaces the library default custom scrollbar handle image with the one of your choice.
     * @param resId
     */
    public void setScrollbarHandleCustomDrawable(int resId) {
        mScrollbarHandle.setBackgroundResource(resId);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mTotalHeight = h;
    }

    private int determineScrollPosition(RecyclerView recyclerView) {
        View firstVisibleView = recyclerView.getChildAt(0);
        int firstVisiblePosition = recyclerView.getChildLayoutPosition(firstVisibleView);
        int itemCount = recyclerView.getAdapter().getItemCount();
        int visibleItemCount = recyclerView.getChildCount();

        float factor = firstVisiblePosition * ((float) visibleItemCount / (float) itemCount);

        if (firstVisiblePosition == 0) {
            return 0;
        } else if (firstVisiblePosition + visibleItemCount >= itemCount) {
            return itemCount;
        } else {
            return (int) Math.ceil(firstVisiblePosition + factor);
        }
    }

    private void setScrollerPosition(int scrollPosition, int itemCount) {
        float positionAsPercentage = (float) scrollPosition / (float) itemCount;

        if (mIsScrollbarEnabled) {
            mScroller.setY((mTotalHeight - mScroller.getHeight()) * positionAsPercentage);
        }

        if (mIsHandleEnabled) {
            mScrollbarHandle.setY((mTotalHeight - mScrollbarHandle.getHeight()) * positionAsPercentage);
        }
    }

    class ScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int scrollPosition = determineScrollPosition(recyclerView);
            setScrollerPosition(scrollPosition, recyclerView.getAdapter().getItemCount());
        }
    }
}
