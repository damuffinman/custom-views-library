package animation;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class ResizeView extends Animation {

    private View mView;
    private int mInitialHeight, mDeltaHeight;
    private int mInitialWidth, mDeltaWidth;

    public ResizeView(View v) {
        mView = v;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        mView.getLayoutParams().height = (int) (mInitialHeight + (mDeltaHeight * interpolatedTime));
        if (mDeltaWidth != 0) {
            mView.getLayoutParams().width = (int) (mInitialWidth + (mDeltaWidth * interpolatedTime));
        }
        mView.requestLayout();
    }

    public void setWidth(int initialWidth, int endWidth) {
        mInitialWidth = initialWidth;
        mDeltaWidth = endWidth - initialWidth;
    }

    public void setHeight(int initialHeight, int endHeight) {
        mInitialHeight = initialHeight;
        mDeltaHeight = endHeight - initialHeight;
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}
