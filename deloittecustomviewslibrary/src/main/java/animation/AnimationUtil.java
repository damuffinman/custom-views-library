package animation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.transition.ChangeBounds;
import android.transition.ChangeTransform;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;

import com.uk.deloitte.digital.customviewslib.R;

public class AnimationUtil {

    private static final long ANIMATION_DURATION = 1000;
    private static final int CROSSFADE_ANIMATION_DURATION = 800;
    private static final int RESIZE_VIEW_DURATION = 300;
    private static final int ANIMATE_VIEW_SCALE_DURATION = 1000;

    public interface AnimationActionListener {
        void onAnimationEnd();
    }

    /**
     * Applies the circular reveal animation to a view. Only for Lollipop+ (API 21+)
     * @param view
     * @param listener Implement this listener to listen to when the animation ends. Pass null otherwise.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void circularReveal(View view, final AnimationActionListener listener) {
        if (view.getVisibility() != View.VISIBLE) {
            int centerX = view.getMeasuredWidth() / 2;
            int centerY = view.getMeasuredHeight() / 2;
            int endRadius = Math.max(view.getWidth(), view.getHeight()) / 2;

            Animator animator = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, 0, endRadius);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(ANIMATION_DURATION);

            view.setVisibility(View.VISIBLE);

            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                    if (listener != null) {
                        listener.onAnimationEnd();
                    }
                }
            });

            animator.start();
        }
    }

    /**
     * Applies the circular hide animation to a view. Only for Lollipop+ (API 21+)
     * @param view
     * @param listener Implement this listener to listen to when the animation ends. Pass null otherwise.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void circularHide(final View view, final AnimationActionListener listener) {
        if (view.getVisibility() == View.VISIBLE) {
            int centerX = view.getMeasuredWidth() / 2;
            int centerY = view.getMeasuredHeight() / 2;
            int initialRadius = view.getWidth() / 2;

            Animator animator = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, initialRadius, 0);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(ANIMATION_DURATION);

            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.INVISIBLE);

                    if (listener != null) {
                        listener.onAnimationEnd();
                    }
                }
            });

            animator.start();
        }
    }

    /**
     * Crossfades transition for the 2 views passed to the method.
     * @param currentlyHiddenView
     * @param currentlyVisibleView
     */
    public static void crossfadeView(View currentlyHiddenView, final View currentlyVisibleView) {
        currentlyHiddenView.setAlpha(0f);
        currentlyHiddenView.setVisibility(View.VISIBLE);

        currentlyHiddenView.animate().alpha(1f).setDuration(CROSSFADE_ANIMATION_DURATION).setListener(null);
        currentlyVisibleView.animate().alpha(0f).setDuration(CROSSFADE_ANIMATION_DURATION).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                currentlyVisibleView.setVisibility(View.GONE);
            }
        });
    }

    /**
     * Resizes the view from the original width/height to newWidth/newHeight over a set duration.
     * @param view
     * @param newHeight
     * @param newWidth
     * @param listener
     */
    public static void animateViewResize(View view, int newHeight, int newWidth, Animation.AnimationListener listener) {
        int initialHeight = view.getHeight();
        int initialWidth = view.getWidth();

        ResizeView resizeView = new ResizeView(view);
        resizeView.setHeight(initialHeight, newHeight);
        resizeView.setWidth(initialWidth, newWidth);
        resizeView.setDuration(RESIZE_VIEW_DURATION);

        resizeView.setAnimationListener(listener);

        view.startAnimation(resizeView);
    }

    /**
     * Animates the view in by scaling from 0% to 100% from the center of the view. This method
     * scales the view from width/height of 0 to the width/height of the view.
     * @param view
     * @param listener
     */
    public static void animateViewInAndScale(View view, AnimatorListenerAdapter listener) {
        animateViewInAndScale(view, false, 0, 0, listener);
    }

    /**
     * Animates the view in by scaling from 0% to 100% from the center of the view. This method
     * scales the view from width/height of 0 to a provided newWidth and newHeight dimensions.
     * @param view
     * @param newWidth
     * @param newHeight
     * @param listener
     */
    public static void animateViewInAndScale(View view, int newWidth, int newHeight, AnimatorListenerAdapter listener) {
        animateViewInAndScale(view, true, newWidth, newHeight, listener);
    }

    private static void animateViewInAndScale(final View view, boolean shouldResizeView, int newWidth, int newHeight, final AnimatorListenerAdapter listener) {

        if (shouldResizeView) {
            animateViewResize(view, newHeight, newWidth, new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    setViewScaleAndVisiblity(view, true);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    animateViewScale(view, true, listener);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
        } else {
            setViewScaleAndVisiblity(view, true);
            animateViewScale(view, true, listener);
        }
    }

    /**
     * Animates the view out by scaling from 100% to 0% from the center of the view.
     * @param view
     * @param shouldCollapseView True if the view should collapse, false if view just becomes invisible
     * @param listener
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void animateViewOutAndScale(final View view, final boolean shouldCollapseView, final AnimatorListenerAdapter listener) {
        AnimatorListenerAdapter animatorListenerAdapter = new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                listener.onAnimationEnd(animation);

                if (shouldCollapseView) {
                    animateViewResize(view, 0, view.getWidth(), new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            setViewScaleAndVisiblity(view, false);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                } else {
                    view.setVisibility(View.INVISIBLE);
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.animate().scaleX(0f).scaleY(0f).translationZ(10f).
                    setInterpolator(new FastOutSlowInInterpolator()).
                    setStartDelay(200).
                    setDuration(ANIMATE_VIEW_SCALE_DURATION).
                    setListener(animatorListenerAdapter).
                    start();
        } else {
            view.animate().scaleX(0f).scaleY(0f).
                    setInterpolator(new FastOutSlowInInterpolator()).
                    setStartDelay(200).
                    setDuration(ANIMATE_VIEW_SCALE_DURATION).
                    setListener(animatorListenerAdapter).
                    start();
        }
    }

    /**
     * Animates the view in from the left side of the screen. This method will push all views in the
     * layout down to display an empty space, before introducing the view from the left hand side.
     * @param view
     * @param context
     * @param newWidth
     * @param newHeight
     */
    public static void animateViewInFromLeft(final View view, final Context context, int newWidth, int newHeight) {
        animateViewResize(view, newHeight, newWidth, new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                setViewScaleAndVisiblity(view, true);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // reset the view to original scale
                view.setScaleY(1f);
                view.setScaleX(1f);

                Animation anim = AnimationUtils.loadAnimation(context, R.anim.slide_in_left);
                view.startAnimation(anim);
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * Animates the view out to the left side of the screen. After the view exits via the left, the
     * remaining views in the layout will collapse upwards to remove the empty space left by the view.
     * @param view
     * @param context
     * @param listener
     */
    public static void animateViewOutToLeft(final View view, Context context, final Animation.AnimationListener listener) {
        Animation.AnimationListener animationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setScaleY(0f);
                view.setScaleX(0f);

                listener.onAnimationEnd(animation);

                animateViewResize(view, 0, view.getWidth(), new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        setViewScaleAndVisiblity(view, false);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };

        Animation anim = AnimationUtils.loadAnimation(context, R.anim.slide_out_left);
        anim.setAnimationListener(animationListener);
        view.startAnimation(anim);
    }

    private static void setViewScaleAndVisiblity(View view, boolean showView) {
        if (showView) {
            view.setVisibility(View.VISIBLE);
            view.setScaleY(0f);
            view.setScaleX(0f);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static void animateViewScale(View view, boolean showView, Animator.AnimatorListener listener) {
        if (showView) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view.animate().scaleX(1f).scaleY(1f).translationZ(10f).
                        setInterpolator(new FastOutSlowInInterpolator()).
                        setStartDelay(200).
                        setDuration(ANIMATE_VIEW_SCALE_DURATION).
                        setListener(listener).
                        start();
            } else {
                view.animate().scaleX(1f).scaleY(1f).
                        setInterpolator(new FastOutSlowInInterpolator()).
                        setStartDelay(200).
                        setDuration(ANIMATE_VIEW_SCALE_DURATION).
                        setListener(listener).
                        start();
            }
        }
    }

    /**
     * Use this in conjunction with the {@link Fragment#setSharedElementEnterTransition(Transition)}
     * method to get a shared element to smoothly transition to the new location in the new fragment.
     *
     * Note this does not do any animations/transitions for non shared elements.
     * @param animationDuration Duration of the animation
     * @return
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static Transition getSharedElementTransition(long animationDuration) {
        TransitionSet transitionSet = new TransitionSet();

        // THIS IS MANDATORY - it remembers the position of the image before the changebound happens
        ChangeTransform changeTransform = new ChangeTransform();

        ChangeBounds bounds = new ChangeBounds();
        bounds.setInterpolator(new DecelerateInterpolator());
        bounds.setDuration(animationDuration);

        transitionSet.addTransition(changeTransform).addTransition(bounds);

        return transitionSet;
    }
}
