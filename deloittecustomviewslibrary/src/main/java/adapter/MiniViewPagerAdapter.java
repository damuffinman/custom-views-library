package adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uk.deloitte.digital.customviewslib.R;

import java.util.List;

import util.CustomLogger;
import util.DisplayUtils;

/**
 * The PagerAdapter to be used with the MiniViewPager
 * @param <T> the data type to be displayed on each page. The .toString() method of this type will
 *           be used to display it.
 */
public class MiniViewPagerAdapter<T> extends PagerAdapter {

    private static final String TAG = MiniViewPagerAdapter.class.getSimpleName();
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private List<T> mValues;
    private View.OnTouchListener mItemOnTouchListener;
    private CustomLogger mLog;

    public MiniViewPagerAdapter(List<T> values, Context context){
        mValues = values;
        mContext = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mValues.size();
    }

    @Override
    public Object instantiateItem(ViewGroup viewpager, int position) {

        View view = mLayoutInflater.inflate(R.layout.miniviewpager_element, viewpager, false);

        TextView tv = (TextView) view;
        tv.setText(mValues.get(position).toString());
        tv.setTag(position);
        setTextColor(tv, position);
        setListenerForItemViewTouch(tv);
        viewpager.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    /**
     * Sets the listener to be used when the user interacts with a page
     * @param listener
     */
    public void setItemTouchListener(View.OnTouchListener listener){
        mItemOnTouchListener = listener;
    }

    /**
     * Sets the color of the textview. Assumes that the first in the first position is selected, and
     * therefore should be styled as such.
     * @param tv The TextView for which the color should be set
     * @param position The position that the textview is in in the list of values
     */
    private void setTextColor(TextView tv, int position){
        if(position == 0){
            int primaryThemeColor = DisplayUtils.getAttributeDataFromCurrentTheme(tv.getContext(), R.attr.colorPrimary);
            tv.setTextColor(primaryThemeColor);
        }
      /*  else{
            int color = DisplayUtils.getAttributeDataFromCurrentTheme(tv.getContext(), android.R.attr.textColor);
            tv.setTextColor(color);
        } */
    }

    /**
     * Adds this view to the list of views being listened to for user interaction
     * @param itemView The view to set the touchlistener for
     */
    private void setListenerForItemViewTouch(View itemView){
        if(mItemOnTouchListener != null) {
            itemView.setOnTouchListener(mItemOnTouchListener);
        }
    }
}
