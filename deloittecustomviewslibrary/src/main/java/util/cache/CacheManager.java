package util.cache;


import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import java.util.List;
import java.util.concurrent.TimeUnit;
import util.logging.LoggerFactory;

/**
 * Build caches using a given expiry and loader, registering them with the manager so they can be invalidated in the
 * future.
 *
 * @author pcornish
 */
public class CacheManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(CacheManager.class);

    /**
     * This class is a singleton.
     */
    private static CacheManager instance;

    /**
     * Requires synchronised method access to this variable to prevent a
     * ConcurrentModificationException when iterating and adding a new cache from different threads.
     */
    private List<Cache> caches = Lists.newArrayList();

    /**
     * @return a singleton instance of the CacheManager.
     */
    public synchronized static CacheManager getInstance() {
        if (null == instance) {
            instance = new CacheManager();
        }
        return instance;
    }

    /**
     * External instance creation not permitted. Use {@link #getInstance()} instead.
     */
    private CacheManager() {
    }

    /**
     * Build a loading cache with the given expiry and loader, registering it with the manager.
     *
     * @param expiry
     *            duration the length of time after an entry is created that it should be automatically removed
     * @param timeUnit
     *            the unit that expiry is expressed in
     * @param cacheLoader
     *            the loader for the read-through cache
     * @return a new LoadingCache
     */
    public synchronized LoadingCache buildCache(long expiry, TimeUnit timeUnit, CacheLoader cacheLoader) {
        final LoadingCache cache = CacheBuilder.newBuilder().expireAfterWrite(expiry, timeUnit).build(cacheLoader);

        LOGGER.debug("Registered loading cache for future invalidation");
        caches.add(cache);

        return cache;
    }

    /**
     * Build a non-loading cache with the given expiry, registering it with the manager.
     *
     * @param expiry
     *            duration the length of time after an entry is created that it should be automatically removed
     * @param timeUnit
     *            the unit that expiry is expressed in
     * @return a new non-loading Cache
     */
    public synchronized Cache buildCache(long expiry, TimeUnit timeUnit) {
        final Cache<Object, Object> cache = CacheBuilder.newBuilder().expireAfterWrite(expiry, timeUnit).build();

        LOGGER.debug("Registered non-loading cache for future invalidation");
        caches.add(cache);

        return cache;
    }

    /**
     * Invalidate all caches registered with the manager.
     */
    public synchronized void invalidateCaches() {
        LOGGER.debug(String.format("Invalidating %s caches", caches.size()));

        for (Cache cache : caches) {
            cache.invalidateAll();
        }
    }

    /**
     * @return the number of registered caches
     */
    public synchronized int getCacheCount() {
        return caches.size();
    }
}
