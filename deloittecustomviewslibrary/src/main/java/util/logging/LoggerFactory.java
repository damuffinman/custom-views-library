package util.logging;

import org.slf4j.Logger;

/**
 * Provides a logger named with the simple name of the class provided, allowing for the 23 character
 * limit Android imposes on log tags.
 *
 * @author pcornish
 */
public class LoggerFactory {
    /**
     * @param clazz {@link Class} for which a logger should be obtained
     * @return a {@link org.slf4j.Logger} for the given {@link Class}
     */
    public static Logger getLogger(Class<?> clazz) {
        return org.slf4j.LoggerFactory.getLogger(clazz.getSimpleName());
    }
}
