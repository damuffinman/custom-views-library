package view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Custom LinearLayout which acts as a container, allowing child views to expand/hide.
 * Expansion/hiding is done in a vertical manner!
 */
public class ExpandVerticalView extends LinearLayout {

    private int mCurrentHeight;
    private int mMaxHeight;

    private Runnable mAnimateRunnable;
    private Handler mHandler;

    private boolean mIsAnimating;

    private static final int HEIGHT_INCREMENT = 8;
    private static final int ANIMATE_FRAME_RATE = 5;

    private State mState;

    private enum State {
        CLOSED, OPEN, ANIMATING_OPEN, ANIMATING_CLOSE
    }

    public ExpandVerticalView(Context context, AttributeSet attr) {
        super(context, attr);

        mHandler = new Handler();
        setOrientation(VERTICAL);
        setWillNotDraw(false);

        reset();

        animate(ANIMATE_FRAME_RATE);
    }

    private void open() {
        if (!mIsAnimating) {
            mIsAnimating = true;
            mState = State.ANIMATING_OPEN;
            setVisibility(VISIBLE);
            mAnimateRunnable.run();
        }
    }

    private void close() {
        if (!mIsAnimating) {
            mIsAnimating = true;
            mState = State.ANIMATING_CLOSE;
            mAnimateRunnable.run();
        }
    }

    /**
     * Expands or closes the view
     */
    public void toggle() {
        if (mState == State.OPEN) {
            close();
        } else if (mState == State.CLOSED) {
            open();
        }
    }

    private void animate(final int frameRate) {
        mAnimateRunnable = new Runnable() {
            @Override
            public void run() {
                if (mState == State.ANIMATING_OPEN) {
                    animateOpen();
                } else if (mState == State.ANIMATING_CLOSE) {
                    animateClose();
                }

                if (mIsAnimating) {
                    mHandler.postDelayed(this, frameRate);
                }
            }
        };
    }

    private void animateOpen() {
        mCurrentHeight += HEIGHT_INCREMENT;

        if (isFullyOpen()) {
            mCurrentHeight = mMaxHeight;
            mIsAnimating = false;
            mState = State.OPEN;
        }

        requestLayout();
    }

    private void animateClose() {
        mCurrentHeight -= HEIGHT_INCREMENT;

        if (isFullyClosed()) {
            reset();
        }

        requestLayout();
    }

    private boolean isFullyOpen() {
        return mCurrentHeight >= mMaxHeight && mMaxHeight != 0;
    }

    private boolean isFullyClosed() {
        return mCurrentHeight <= 0;
    }

    /**
     * Resets the view to the original closed state
     */
    public void reset() {
        mCurrentHeight = 0;
        mIsAnimating = false;
        mState = State.CLOSED;
        setVisibility(GONE);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);

        int height = 0;
        for (int i = 0; i < getChildCount(); i++) {
            View childView = getChildAt(i);

            if (childView.getVisibility() != GONE) {
                measureChild(childView, widthMeasureSpec, heightMeasureSpec);
                height += childView.getMeasuredHeight();
                height += getVerticalMargins(childView.getLayoutParams());
            }
        }

        height += getPaddingBottom() + getPaddingTop();
        mMaxHeight = height;

        setMeasuredDimension(parentWidth, mCurrentHeight);
    }

    private int getVerticalMargins(ViewGroup.LayoutParams layoutParams) {
        int margins = 0;

        if (layoutParams instanceof LinearLayout.LayoutParams) {
            LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) layoutParams;
            margins += linearParams.topMargin;
            margins += linearParams.bottomMargin;
        } else if (layoutParams instanceof FrameLayout.LayoutParams) {
            FrameLayout.LayoutParams frameParams = (FrameLayout.LayoutParams) layoutParams;
            margins += frameParams.topMargin;
            margins += frameParams.bottomMargin;
        } else if (layoutParams instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams relativeParams = (RelativeLayout.LayoutParams) layoutParams;
            margins += relativeParams.topMargin;
            margins += relativeParams.bottomMargin;
        }

        return margins;
    }
}
