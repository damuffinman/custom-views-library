package view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.uk.deloitte.digital.customviewslib.R;

import adapter.MiniViewPagerAdapter;
import util.CustomLogger;
import util.DisplayUtils;

/**
 * Custom viewpager which manages styles for selected pages.
 */
public class MiniViewPager extends ViewPager {

    private Context mContext;
    private ColorStateList mDefaultItemViewColors;
    private CustomLogger mLog;

    public MiniViewPager(Context context) {
        super(context);
        init(context);
    }

    public MiniViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    /**
     * If the adapter is of type MiniViewPagerAdapter, the clicklistener will be set up.
     * Adapter is then set up as normal for a viewpager.
     *
     * @param adapter The adapter to be used for the viewpager.
     */
    @Override
    public void setAdapter(PagerAdapter adapter){
        if(adapter != null && adapter instanceof MiniViewPagerAdapter){
            setupItemClickListener((MiniViewPagerAdapter) adapter);
        }
        else{
            mLog.w("Use a " + MiniViewPagerAdapter.class.getName()
                    + " to ensure all features are available");
        }
        super.setAdapter(adapter);
    }

    /**
     * Initialises the viewpager
     *
     * @param context The relevant activity's context
     */
    private void init(Context context){
        mContext = context;
        mLog = new CustomLogger(true, getClass().getSimpleName());
        addOnPageChangeListener(new SelectedPageListener());
        setOffscreenPageLimit(50);
    }

    /**
     * Sets listener for when the user interacts with the pager
     *
     * @param miniViewPagerAdapter The adapter being used for the view pager. Must be of type
     *                             MiniViewPagerAdapter.
     */
    private void setupItemClickListener(MiniViewPagerAdapter miniViewPagerAdapter){
        miniViewPagerAdapter.setItemTouchListener(new PageItemOnTouchListener());
    }

    /**
     * Called when a page in the viewpager is selected.
     * Updates the styling for the selected page and removes 'selected' styling from previously
     * selected page.
     *
     * @param selectedPageNum The index number of the page that has been selected
     */
    private void pageSelected(int selectedPageNum){
        int pageCount = getAdapter().getCount();
        backUpDefaultTextColors((TextView) findViewWithTag(selectedPageNum));
        for(int pageNum = 0; pageNum < pageCount; pageNum++){
            TextView pageTextView = (TextView) findViewWithTag(pageNum);
            if(pageTextView != null) {
                if (pageNum == selectedPageNum) {
                    int primaryThemeColor = DisplayUtils.getAttributeDataFromCurrentTheme(mContext, R.attr.colorPrimary);
                    pageTextView.setTextColor(primaryThemeColor);
                } else {
                    pageTextView.setTextColor(mDefaultItemViewColors);
                }
            }
        }
    }

    /**
     * Stores the default colors of a textview before the library changes any colors.
     * This is to retrieve the theme-based colors so we can revert back to them if necessary.
     *
     * @param textView A textview from the pager with colors that have not yet been changed
     */
    private void backUpDefaultTextColors(TextView textView){
        if(mDefaultItemViewColors == null) {
            mDefaultItemViewColors = textView.getTextColors();
        }
    }

    /**
     * Listens for when the page in a pager is changed, and calls the appropriate method.
     */
    private class SelectedPageListener implements OnPageChangeListener{

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

        @Override
        public void onPageSelected(int position) {
            pageSelected(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {}
    }

    /**
     * Detects when the user presses on an unselected page. When this happens, the pressed
     * page will be programmatically selected.
     *
     * Ignores presses with motion (such as swipes).
     */
    private class PageItemOnTouchListener implements OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            boolean eventIsRelevant = false;

            if(event.getAction() == MotionEvent.ACTION_DOWN){
                eventIsRelevant = true;
            }
            else if(event.getAction() == MotionEvent.ACTION_UP){
                setCurrentItem((int) v.getTag());
                eventIsRelevant = true;
            }

            return eventIsRelevant;
        }
    }
}
