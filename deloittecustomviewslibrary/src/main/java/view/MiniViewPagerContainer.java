package view;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.uk.deloitte.digital.customviewslib.R;

import util.DisplayUtils;

/**
 * Custom view to be used for creating the MiniViewPager.
 * Layout consists of a view pager and an image overlay for the circle around the selected item.
 * The view pager and image are wrapped in a frame layout.
 */
public class MiniViewPagerContainer extends RelativeLayout {

    // TODO Get this to work
    private static final int DEFAULT_TEXTVIEW_WIDTH_DP = 30;

    private FrameLayout mImageOverlayWrapper;
    private MiniViewPager mMiniViewPager;
    private ImageView mImageOverlayForSelectedPage;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private GradientDrawable mImageGradientDrawable;
    private AttributeSet mAttributes;

    private int paddingBetweenTextViews = 30;

    public MiniViewPagerContainer(Context context) {
        super(context);
        init(context);
    }

    public MiniViewPagerContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
        mAttributes = attrs;
        init(context);
    }

    public MiniViewPagerContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mAttributes = attrs;
        init(context);
    }

    /**
     * @return The viewpager being used to display the pages
     */
    public ViewPager getViewPager(){
        return this.mMiniViewPager;
    }

    /**
     * Initialises viewpager container and children with default size determined for the width of
     * the textview for each page
     * @param context The relevant activity's context
     */
    private void init(Context context){
        int defaultTextViewWidthPx = DisplayUtils.convertDpToPx(DEFAULT_TEXTVIEW_WIDTH_DP);
        init(context, defaultTextViewWidthPx);
    }

    /**
     * Initialises viewpager container and children
     * @param context The relevant activity's context
     * @param textWidthPx the width of the textviews used by each page
     */
    private void init(Context context, int textWidthPx){
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        createMiniViewPager(textWidthPx);
        createImageOverlay();
        addViews();
    }

    /**
     * Creates the view pager with the required configuration in order for it to display as expected.
     * @param textViewWidthPx The width of the textview for each page. This is used in deciding the
     *                        amounnt of padding to use.
     */
    private void createMiniViewPager(int textViewWidthPx){
        mMiniViewPager = (MiniViewPager) mLayoutInflater.inflate(R.layout.miniviewpager, this, false);
        mMiniViewPager.setClipToPadding(false);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

        int padding = Math.round( ((displayMetrics.widthPixels - textViewWidthPx) / 2)  - paddingBetweenTextViews);
        mMiniViewPager.setPadding(padding, 0, padding, 0);
    }

    /**
     * Creates the image overlay which will display the shape around the selected page to highlight it
     */
    private void createImageOverlay(){
        mImageOverlayWrapper = (FrameLayout) mLayoutInflater.inflate(R.layout.miniviewpager_imageoverlap, this, false);
        setImageOverlayoutWrapperLayoutParams();

        mImageOverlayForSelectedPage = (ImageView) mImageOverlayWrapper.findViewById(R.id.miniviewpagerimage);
        GradientDrawable gd = createGradientDrawableForImageOverlay();
        mImageOverlayForSelectedPage.setImageDrawable(gd);
    }

    /**
     * Sets the layout parameters for the image overlay so that it exactly overlaps with the viewpager
     */
    private void setImageOverlayoutWrapperLayoutParams(){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mImageOverlayWrapper.getLayoutParams();

        layoutParams.addRule(RelativeLayout.ALIGN_LEFT, mMiniViewPager.getId());
        layoutParams.addRule(RelativeLayout.ALIGN_RIGHT, mMiniViewPager.getId());
        layoutParams.addRule(RelativeLayout.ALIGN_TOP, mMiniViewPager.getId());
        layoutParams.addRule(RelativeLayout.ALIGN_BOTTOM, mMiniViewPager.getId());

        mImageOverlayWrapper.setLayoutParams(layoutParams);
    }

    /**
     * Creates the shape to display over the selected page
     * @return the shape to display over the selected page
     */
    private GradientDrawable createGradientDrawableForImageOverlay(){
        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.OVAL);
        gd.setCornerRadius(5);
        int strokeColor = DisplayUtils.getAttributeDataFromCurrentTheme(getContext(), R.attr.colorPrimary);
        gd.setStroke(8, strokeColor);
        mImageGradientDrawable = gd;
        return mImageGradientDrawable;
    }

    /**
     * Adds the child views to this container view.
     */
    private void addViews(){
        addView(mMiniViewPager);
        addView(mImageOverlayWrapper);
    }

}