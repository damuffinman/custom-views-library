package view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.uk.deloitte.digital.customviewslib.R;

/**
 * Created by kekwok on 29/04/16.
 */
public class DialogView extends LinearLayout {

    private boolean mIsDismissOnOutsideClick;

    public DialogView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.DialogView, 0, 0);
        try {
            mIsDismissOnOutsideClick = ta.getBoolean(R.styleable.DialogView_dismissOnOutsideClick, false);
        } finally {
            ta.recycle();
        }

        getRootView().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIsDismissOnOutsideClick) {
                    setVisibility(View.INVISIBLE);
                }
            }
        });

        // this is to ensure that any clicks on the child view does not dismiss the dialog
        if (((ViewGroup) getRootView()).getChildCount() > 0) {
            ((ViewGroup) getRootView()).getChildAt(0).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }
}
