package com.uk.deloitte.digital.customviewslib.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.uk.deloitte.digital.customviewslib.service.BaseServiceTest;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import util.cache.CacheManager;

/**
 * Tests for {@link util.cache.CacheManager}.
 *
 * @author pcornish
 */
public class CacheManagerTest extends BaseServiceTest {

    private static final long EXPIRY = 1000;
    private static final Object TEST_OBJECT = new Object();
    private static final String TEST_KEY = "key";

    /**
     * Class under test.
     */
    private CacheManager cacheManager;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        cacheManager = CacheManager.getInstance();

        // clear before running
        cacheManager.invalidateCaches();
        setFieldOnObject(cacheManager, "caches", Lists.newArrayList());
    }

    /**
     * Create a single non-loading cache.
     */
    @SuppressWarnings("unchecked")
    public void testBuildCache_NonLoading_Success() {
        // sanity check
        assertEquals("Cache manager should be empty", 0, cacheManager.getCacheCount());

        // call
        final Cache<String, Object> actual = cacheManager.buildCache(EXPIRY, TimeUnit.SECONDS);

        // assert output
        assertNotNull("Cache should be created", actual);
        assertEquals("One cache should be present in the CacheManager", 1, cacheManager.getCacheCount());

        // verify contents
        assertNull("Cache should not contain value", actual.getIfPresent("NoValueForKey"));
        actual.put(TEST_KEY, TEST_OBJECT);
        assertEquals("Cache should retain manually inserted value", TEST_OBJECT, actual.getIfPresent(TEST_KEY));
    }

    /**
     * Create a single loading cache.
     */
    @SuppressWarnings("unchecked")
    public void testBuildCache_Loading_Success() throws ExecutionException {
        // test data
        final CacheLoader<String, Object> cacheLoader = new CacheLoader<String, Object>() {
            @Override
            public Object load(String key) throws Exception {
                if (TEST_KEY.equals(key)) {
                    return TEST_OBJECT;
                }
                return null;
            }
        };

        // sanity check
        assertEquals("Cache manager should be empty", 0, cacheManager.getCacheCount());

        // call
        final LoadingCache<String, Object> actual = cacheManager.buildCache(EXPIRY, TimeUnit.SECONDS, cacheLoader);

        // assert output
        assertNotNull("Cache should be created", actual);
        assertEquals("One cache should be present in the CacheManager", 1, cacheManager.getCacheCount());

        // verify contents
        assertNull("Cache should not contain value", actual.getIfPresent("NoValueForKey"));
        assertEquals("Cache should return value from CacheLoader", TEST_OBJECT, actual.get(TEST_KEY));
    }

    /**
     * Invalidate cache contents.
     */
    @SuppressWarnings("unchecked")
    public void testInvalidateCache_NonLoading_Success() {
        // sanity check
        assertEquals("Cache manager should be empty", 0, cacheManager.getCacheCount());

        // setup
        final Cache<String, Object> cache1 = cacheManager.buildCache(EXPIRY, TimeUnit.SECONDS);
        final Cache<String, Object> cache2 = cacheManager.buildCache(EXPIRY, TimeUnit.SECONDS);

        // assert output
        assertNotNull("Cache should be created", cache1);
        assertNotNull("Cache should be created", cache2);
        assertEquals("Two caches should be present in the CacheManager", 2, cacheManager.getCacheCount());

        // set up data
        cache1.put(TEST_KEY, TEST_OBJECT);
        assertEquals("Cache should retain manually inserted value", TEST_OBJECT, cache1.getIfPresent(TEST_KEY));

        cache2.put(TEST_KEY, TEST_OBJECT);
        assertEquals("Cache should retain manually inserted value", TEST_OBJECT, cache2.getIfPresent(TEST_KEY));

        // invalidate and verify
        cacheManager.invalidateCaches();
        assertEquals("Two caches should still be present in the CacheManager", 2, cacheManager.getCacheCount());
        assertNull("Cache should no longer contain inserted value", cache1.getIfPresent(TEST_KEY));
        assertNull("Cache should no longer contain inserted value", cache2.getIfPresent(TEST_KEY));
    }
}
