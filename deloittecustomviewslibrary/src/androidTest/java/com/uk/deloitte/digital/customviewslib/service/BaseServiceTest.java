package com.uk.deloitte.digital.customviewslib.service;

import android.test.InstrumentationTestCase;

import java.lang.reflect.Field;

/**
 * @author pcornish
 */
public abstract class BaseServiceTest extends InstrumentationTestCase {
    private static final long WAIT_FOR_THREAD_TIMEOUT = 10000;
    private static final String FIELD_NAME_CONTEXT = "context";

    /**
     * Set the field with the given name to the specified value.
     *
     * @param target
     * @param fieldName
     * @param fieldValue
     */
    protected void setFieldOnObject(Object target, String fieldName, Object fieldValue) {
        try {
            final Field declaredField = getFieldRecursive(target.getClass(), fieldName);

            final boolean isAccessible = declaredField.isAccessible();
            if (!isAccessible) {
                declaredField.setAccessible(true);
            }

            declaredField.set(target, fieldValue);

            // reset accessibility
            if (!isAccessible) {
                declaredField.setAccessible(false);
            }

        } catch (Exception e) {
            throw new RuntimeException(String.format("Error setting field %s to value %s", fieldName, fieldValue), e);
        }
    }

    /**
     * Get a field from the specified class or any of its superclasses.
     *
     * @param clazz
     *            the Class to check for the field
     * @param name
     *            the name of the Field to find
     * @return the Field on the given Class matching the <code>name</code>
     * @throws NoSuchFieldException
     */
    private Field getFieldRecursive(Class<?> clazz, String name) throws NoSuchFieldException {
        Field result = null;
        try {
            result = clazz.getDeclaredField(name);
        } catch (NoSuchFieldException e) {
            // sink - keep searching
        }

        if (result != null) {
            return result;
        }
        // search superclass
        if (null != clazz.getSuperclass()) {
            Field superclassResult = null;
            try {
                superclassResult = getFieldRecursive(clazz.getSuperclass(), name);
            } catch (NoSuchFieldException e) {
                // sink - keep searching
            }

            if (superclassResult != null) {
                return superclassResult;
            }
        }

        throw new NoSuchFieldException("Field " + name + " not present on class " + clazz + " or superclasses");
    }

    /**
     * Inject the target Context from Instrumentation into the specified object. Note, there must be a field named
     * 'context' declared within the object.
     *
     * @param obj
     *            the Object into which the Context should be injected.
     */
    protected void injectTargetContext(Object obj) {
        setFieldOnObject(obj, FIELD_NAME_CONTEXT, getInstrumentation().getTargetContext());
    }
}
