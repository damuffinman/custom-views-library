package com.uk.deloitte.digital.kekwok.sample;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.TextView;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.assertion.LayoutAssertions.noOverlaps;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.core.AnyOf.anyOf;
import static org.hamcrest.core.IsNot.not;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class MiniViewPagerTest {

    @Rule
    public ActivityTestRule<MiniViewPagerDemoActivity> mActivityRule = new ActivityTestRule<>(MiniViewPagerDemoActivity.class);

    @Test
    public void viewPagerContainerIsDisplayed(){
        onView(withId(R.id.miniviewpagercontainer)).check(matches(isDisplayed()));
    }

    @Test
    public void miniViewPagerIsDisplayed(){
        onView(withId(R.id.minipager)).check(matches(isDisplayed()));
        onView(withId(R.id.minipager)).check(matches(withParent(withId(R.id.miniviewpagercontainer))));
    }

    @Test
    public void pagerItemsCanBeSelected(){
        onView(withText("4")).perform(click());
        onView(withText("Submit Selection")).perform(click());
        onView(withText("Selected value: 4")).check(matches(isDisplayed()));
    }

    @Test
    public void pagerViewsDoNotOverlap(){
        onView(withId(R.id.miniviewpagercontainer)).check(noOverlaps(isAssignableFrom(TextView.class)));
    }

    @Test
    public void imageViewDoesNotAppearOverUnselectedItem(){
        onView(withText("4")).perform(click());
        onView(withId(R.id.miniviewpagercontainer)).check(noOverlaps(anyOf(allOf(isAssignableFrom(TextView.class), not(withText("4"))), withId(R.id.miniviewpagerimage))));
    }

    @Test
    public void clickingPageNotInitiallyShownOnScreen(){
        onView(withId(R.id.minipager)).perform(swipeLeft());
        onView(withText("10")).perform(click());
        onView(withText("Submit Selection")).perform(click());
        onView(withText("Selected value: 10")).check(matches(isDisplayed()));
    }

    @Test
    public void multipleSelections(){
        onView(withId(R.id.minipager)).perform(swipeLeft());
        onView(withText("10")).perform(click());
        onView(withText("9")).perform(click());
        onView(withText("8")).perform(click());
        onView(withId(R.id.minipager)).perform(swipeRight());
        onView(withText("3")).perform(click());
        onView(withId(R.id.viewpagerdemo_submit_btn)).perform(click());
        onView(withText("Selected value: 3")).check(matches(isDisplayed()));
    }

    @Test
    public void everyItemIsSelectable(){
        // 28 is the last value in the dummy data
        manualSwipeFromItemValueToItemValue(1, 28);
        onView(withId(R.id.viewpagerdemo_submit_btn)).perform(click());
        onView(withText("Selected value: 28")).check(matches(isDisplayed()));
    }

    @Test
    public void multiplePagesAreVisibleAtOnce(){
        onView(withText("1")).check(matches(isDisplayed()));
        onView(withText("2")).check(matches(isDisplayed()));
        onView(withText("3")).check(matches(isDisplayed()));
    }

    private void manualSwipeFromItemValueToItemValue(int startItem, int endItem){
        for(int nextItem = startItem + 1; nextItem <= endItem; nextItem++){
            String nextItemText = Integer.toString(nextItem);
            onView(withText(nextItemText)).perform(click());
        }
    }
}
