package interfaces;

import android.view.View;
import nonlibdemo.MarshmallowActivity;

public interface ImageClickListener {
    void click(MarshmallowActivity.Item item, View view);
}
