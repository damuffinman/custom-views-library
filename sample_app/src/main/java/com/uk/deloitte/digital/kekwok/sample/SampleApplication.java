package com.uk.deloitte.digital.kekwok.sample;

import android.app.Application;

import com.scopely.fontain.Fontain;

/**
 * Created by kekwok on 05/12/2016.
 */

public class SampleApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		Fontain.init(this);
	}
}
