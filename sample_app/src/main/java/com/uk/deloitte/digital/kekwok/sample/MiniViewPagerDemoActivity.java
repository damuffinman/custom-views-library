package com.uk.deloitte.digital.kekwok.sample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adapter.MiniViewPagerAdapter;
import view.MiniViewPagerContainer;

public class MiniViewPagerDemoActivity extends AppCompatActivity {

    private static final String TAG = MiniViewPagerAdapter.class.getSimpleName();

    private MiniViewPagerContainer mMiniViewPagerContainer;
    private Button mSubmit;
    private TextView mSelectedValue;

    private List<Integer> mPagerValues;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miniviewpagerdemo);
        mMiniViewPagerContainer = (MiniViewPagerContainer) findViewById(R.id.miniviewpagercontainer);
        mSubmit = (Button) findViewById(R.id.viewpagerdemo_submit_btn);
        mSelectedValue = (TextView) findViewById(R.id.viewpagerdemo_selectedvalue_tv);
        initPager();
        setButtonListener();
    }

    private void initPager(){
        mPagerValues = generateFakeData();
        ViewPager miniViewPager = mMiniViewPagerContainer.getViewPager();
        miniViewPager.setAdapter(new MiniViewPagerAdapter(mPagerValues, this));
    }

    private void setButtonListener(){
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewPager pager = mMiniViewPagerContainer.getViewPager();
                int currentItemPosition = pager.getCurrentItem();
                int selectedItem = mPagerValues.get(currentItemPosition);
                mSelectedValue.setText("Selected value: " + selectedItem);
            }
        });
    }

    private List<Integer> generateFakeData(){
        List<Integer> data = new ArrayList<>();
        for(int i = 0; i < 28; i++){
            data.add(i + 1);
        }
        return data;
    }

}
