package com.uk.deloitte.digital.kekwok.sample;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import animatedrecyclerview.RecyclerViewFastScroller;

public class ListActivity extends Activity {

    private RecyclerView mRecyclerView;
    private ArrayList<String> mSampleListData;
    private ListAdapter mListAdapter;
    private RecyclerViewFastScroller mFastScroller;

    private final int DATA_SIZE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        initListData();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        mFastScroller = (RecyclerViewFastScroller) findViewById(R.id.fastscroller);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mListAdapter = new ListAdapter(this, mSampleListData, mRecyclerView);
        mRecyclerView.setAdapter(mListAdapter);
        mFastScroller.setRecyclerView(mRecyclerView);
    }

    private void initListData() {
        mSampleListData = new ArrayList<>();

        for (int i = 0; i < DATA_SIZE; i++) {
            String string = "Item " + (i+1);
            mSampleListData.add(string);
        }
    }
}
