package com.uk.deloitte.digital.kekwok.sample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import animatedrecyclerview.AnimatedRecyclerViewAdapter;
import nonlibdemo.RecyclerViewScrollListener;
import nonlibdemo.ProgressBarViewHolder;
import nonlibdemo.SampleViewHolder;
import nonlibdemo.BaseViewHolder;

/**
 * Created by kekwok on 10/05/2016.
 */
public class ListAdapter extends AnimatedRecyclerViewAdapter<BaseViewHolder> {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_PROGRESSBAR = 1;

    private RecyclerViewScrollListener mListener;
    private ArrayList<? extends Object> mList;

    public ListAdapter(Context context, ArrayList<? extends Object> list, RecyclerView recyclerView) {
        super(context);
        mList = list;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View text = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_text, parent, false);
        View progressBar = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_progressbar, parent, false);

        if (viewType == VIEW_TYPE_ITEM) {
            return new SampleViewHolder(text);
        } else {
            return new ProgressBarViewHolder(progressBar);
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        if (holder instanceof SampleViewHolder) {
            ((SampleViewHolder) holder).getTextView().setText(mList.get(position).toString());
            alphaInViewOnBind(holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size() + 1;
    }

    public void updateData(ArrayList<? extends Object> list) {
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position >= mList.size() ? VIEW_TYPE_PROGRESSBAR : VIEW_TYPE_ITEM;
    }
}
