package com.uk.deloitte.digital.kekwok.sample;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import view.DialogView;
import view.ExpandVerticalView;

import animation.AnimationUtil;
import nonlibdemo.MarshmallowActivity;
import nonlibdemo.DemoParallaxListActivity;

public class MainActivity extends AppCompatActivity {

    private Button mViewPagerButton;

    private Button mExpandButton;
    private ExpandVerticalView mExpandVerticalView;
    private LinearLayout mCircularContainer;
    private Button mCircularRevealButton, mCircularHideButton;
    private Button mLaunchListActivityButton;
    private Button mLaunchMarshmallowActivityButton;

    private TextView mFillerTextView;
    private ProgressBar mFillerProgressBar;
    private Button mToggleCrossfadeButton;

    private Button mLaunchDialogButton;

    private Button mToggleScaleContentButton;
    private TextView mScaleContentTextView;
    private Button mTransitionContentButton;
    private TextView mTransitionContentTextView;

    private DialogView mDialogView;
    private Button mDialogDismissButton;

    private Button mLaunchParallaxListActivityButton;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.activity_main_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        mViewPagerButton = (Button) findViewById(R.id.viewpager_button);

        mLaunchListActivityButton = (Button) findViewById(R.id.launch_list_activity_button);

        mExpandButton = (Button) findViewById(R.id.expand_button);
        mExpandVerticalView = (ExpandVerticalView) findViewById(R.id.expand_vertical_view);

        mCircularHideButton = (Button) findViewById(R.id.circular_hide_button);
        mCircularRevealButton = (Button) findViewById(R.id.circular_show_button);
        mCircularContainer = (LinearLayout) findViewById(R.id.reveal_layout);

        mLaunchMarshmallowActivityButton = (Button) findViewById(R.id.launch_marshmallow_showoff_activity);

        mFillerTextView = (TextView) findViewById(R.id.crossfade_content);
        mFillerProgressBar = (ProgressBar) findViewById(R.id.crossfade_loader);
        mToggleCrossfadeButton = (Button) findViewById(R.id.toggle_crossfade_button);

        mToggleScaleContentButton = (Button) findViewById(R.id.toggle_scale_button);
        mScaleContentTextView = (TextView) findViewById(R.id.scale_content);
        mTransitionContentButton = (Button) findViewById(R.id.toggle_transition_button);
        mTransitionContentTextView = (TextView) findViewById(R.id.transition_content);

        mLaunchDialogButton = (Button) findViewById(R.id.launch_dialog);
        mDialogView = (DialogView) findViewById(R.id.dialog);
        mDialogDismissButton = (Button) findViewById(R.id.dismiss_button);

        mLaunchParallaxListActivityButton = (Button) findViewById(R.id.launch_parallax_list_activity_button);

        setListeners();
    }

    private void setListeners() {

        mViewPagerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MiniViewPagerDemoActivity.class);
                startActivity(intent);
            }
        });

        mExpandButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mExpandVerticalView.toggle();
            }
        });

        mCircularHideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AnimationUtil.circularHide(mCircularContainer, null);
            }
        });

        mCircularRevealButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AnimationUtil.circularReveal(mCircularContainer, null);
            }
        });

        mLaunchListActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });

        mLaunchParallaxListActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DemoParallaxListActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });

        mLaunchMarshmallowActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MarshmallowActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });

        mToggleCrossfadeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFillerProgressBar.getVisibility() == View.GONE) {
                    AnimationUtil.crossfadeView(mFillerProgressBar, mFillerTextView);
                    AnimationUtil.animateViewResize(mToggleCrossfadeButton, mToggleCrossfadeButton.getHeight(), 420, new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            mToggleCrossfadeButton.setText("Show Content");
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                } else {
                    AnimationUtil.crossfadeView(mFillerTextView, mFillerProgressBar);
                    AnimationUtil.animateViewResize(mToggleCrossfadeButton, mToggleCrossfadeButton.getHeight(), 600, new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            mToggleCrossfadeButton.setText("Show Progress Spinner");
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                }
            }
        });

        mToggleScaleContentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mScaleContentTextView.getVisibility() == View.VISIBLE) {
                    AnimationUtil.animateViewOutAndScale(mScaleContentTextView, true, new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            mToggleScaleContentButton.setText(getText(R.string.animate_and_scale_text));
                        }
                    });
                } else {
                    AnimationUtil.animateViewInAndScale(mScaleContentTextView, mScaleContentTextView.getWidth(), 500, null);
                    mToggleScaleContentButton.setText(getText(R.string.hide_text));
                }
            }
        });

        mTransitionContentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mTransitionContentTextView.getVisibility() == View.VISIBLE) {
                    AnimationUtil.animateViewOutToLeft(mTransitionContentTextView, mContext, new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            mTransitionContentButton.setText(getText(R.string.animate_from_left_text));
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                } else {
                    AnimationUtil.animateViewInFromLeft(mTransitionContentTextView, mContext, mTransitionContentTextView.getWidth(), 500);
                    mTransitionContentButton.setText(getText(R.string.hide_text));
                }
            }
        });

        mLaunchDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchDialog();
            }
        });

        mDialogDismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissDialog();
            }
        });
    }

    private void launchDialog() {
        mDialogView.setVisibility(View.VISIBLE);
    }

    private void dismissDialog() {
        mDialogView.setVisibility(View.INVISIBLE);
    }
}
