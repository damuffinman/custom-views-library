package nonlibdemo;

import android.view.View;
import android.widget.TextView;

import com.uk.deloitte.digital.kekwok.sample.R;

/**
 * Created by kekwok on 10/05/2016.
 */
public class SampleViewHolder extends BaseViewHolder {
    protected TextView textView;

    public SampleViewHolder(View view) {
        super(view, view.findViewById(R.id.vh_container));
        this.textView = (TextView) view.findViewById(R.id.vh_textview);
    }

    public TextView getTextView() {
        return textView;
    }
}
