package nonlibdemo;

import android.app.Activity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.View;

import com.uk.deloitte.digital.kekwok.sample.R;

import animation.AnimationUtil;
import interfaces.ImageClickListener;

public class MarshmallowActivity extends Activity implements ImageClickListener {

    public static final String TRANSITION_PREFIX = "image";

    public enum Item {
        CHROME, TFL, BITCOIN
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marshmallow);

        MarshmallowInitialFragment fragment = new MarshmallowInitialFragment();
        fragment.setListener(this);

        getFragmentManager().beginTransaction().replace(R.id.scene_root, fragment).commit();

    }

    public void click(Item item, View view) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("image", item);

        MarshmallowFocusedFragment fragment = new MarshmallowFocusedFragment();
        fragment.setListener(this);
        fragment.setArguments(bundle);

        fragment.setSharedElementEnterTransition(AnimationUtil.getSharedElementTransition(300));
        fragment.setSharedElementReturnTransition(AnimationUtil.getSharedElementTransition(300));
        fragment.setExitTransition(getNonSharedElementTransition());
        fragment.setEnterTransition(getNonSharedElementTransition());

        getFragmentManager()
                .beginTransaction()
                .addSharedElement(view, TRANSITION_PREFIX + item)
                .replace(R.id.scene_root, fragment)
                .addToBackStack(null)
                .commit();
    }

    public static Transition getNonSharedElementTransition() {
        TransitionSet transitionSet = new TransitionSet();
        Explode explode = new Explode();
        explode.setDuration(800);

        transitionSet.addTransition(explode);

        return transitionSet;
    }
}
