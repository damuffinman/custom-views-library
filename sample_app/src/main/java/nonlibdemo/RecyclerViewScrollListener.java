package nonlibdemo;

public interface RecyclerViewScrollListener {
    void scrolledToRecyclerViewBottom(int lastIndex);
    void scrolledToRecyclerViewTop();
}
