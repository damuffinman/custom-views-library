package nonlibdemo;

import android.view.View;

import animatedrecyclerview.AnimatedRecyclerViewHolder;

public class BaseViewHolder extends AnimatedRecyclerViewHolder {

    public BaseViewHolder(View view, View rootView) {
        super(view, rootView);
    }
}
