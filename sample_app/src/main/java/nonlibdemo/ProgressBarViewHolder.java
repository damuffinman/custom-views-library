package nonlibdemo;

import android.view.View;
import android.widget.ProgressBar;

import com.uk.deloitte.digital.kekwok.sample.R;

/**
 * Created by kekwok on 13/05/2016.
 */
public class ProgressBarViewHolder extends BaseViewHolder {
    protected ProgressBar mProgressBar;

    public ProgressBarViewHolder(View view) {
        super(view, view.findViewById(R.id.vh_container));
        mProgressBar = (ProgressBar) view.findViewById(R.id.vh_progressbar);
    }
}
