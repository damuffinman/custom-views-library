package nonlibdemo;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.uk.deloitte.digital.kekwok.sample.R;

import interfaces.ImageClickListener;

public class MarshmallowFocusedFragment extends Fragment {

    private ImageClickListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
        Best to set the enter/return transitions where you create the fragment rather than onCreate().
        This is an observation from many trial and error attempts
         */

//        setSharedElementEnterTransition(MarshmallowActivity.getSharedElementTransition());
//        setSharedElementReturnTransition(MarshmallowActivity.getSharedElementTransition());

//        setReenterTransition(MarshmallowActivity.getNonSharedElementTransition());
//        setExitTransition(MarshmallowActivity.getNonSharedElementTransition());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_marshmallow_layout_2, container, false);

        // This must be done to ensure the transition name is UNIQUE
        if (getArguments() != null && getArguments().getSerializable("image") != null) {
            ImageView imageView = (ImageView) view.findViewById(R.id.imageview);

            MarshmallowActivity.Item item = (MarshmallowActivity.Item) getArguments().getSerializable("image");
            imageView.setTransitionName(MarshmallowActivity.TRANSITION_PREFIX + item);

            switch (item) {
                case BITCOIN: imageView.setImageResource(R.drawable.bitcoin); break;
                case CHROME: imageView.setImageResource(R.drawable.chrome); break;
                case TFL: imageView.setImageResource(R.drawable.tfl); break;
            }

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().popBackStackImmediate();
                }
            });
        }

        return view;
    }

    public void setListener(ImageClickListener listener) {
        mListener = listener;
    }
}
