package nonlibdemo;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.uk.deloitte.digital.kekwok.sample.R;

import animation.AnimationUtil;
import interfaces.ImageClickListener;

public class MarshmallowInitialFragment extends Fragment {

    private ImageView mChromeImage, mTflImage, mBitcoinImage;
    private ImageClickListener mListener;

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            MarshmallowActivity.Item item;

            switch (view.getId()) {
                case R.id.chrome_imageview:
                    item = MarshmallowActivity.Item.CHROME;
                    break;
                case R.id.bitcoin_imageview:
                    item = MarshmallowActivity.Item.BITCOIN;
                    break;
                case R.id.tfl_imageview:
                    item = MarshmallowActivity.Item.TFL;
                    break;
                default: item = MarshmallowActivity.Item.BITCOIN;
            }

            mListener.click(item, view);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_marshmallow_layout_1, container, false);

        setSharedElementEnterTransition(AnimationUtil.getSharedElementTransition(300));
        setSharedElementReturnTransition(AnimationUtil.getSharedElementTransition(300));
        setExitTransition(MarshmallowActivity.getNonSharedElementTransition());
        setEnterTransition(MarshmallowActivity.getNonSharedElementTransition());

        mChromeImage = (ImageView) view.findViewById(R.id.chrome_imageview);
        mTflImage = (ImageView) view.findViewById(R.id.tfl_imageview);
        mBitcoinImage = (ImageView) view.findViewById(R.id.bitcoin_imageview);

        mBitcoinImage.setTransitionName(MarshmallowActivity.TRANSITION_PREFIX + MarshmallowActivity.Item.BITCOIN);
        mChromeImage.setTransitionName(MarshmallowActivity.TRANSITION_PREFIX + MarshmallowActivity.Item.CHROME);
        mTflImage.setTransitionName(MarshmallowActivity.TRANSITION_PREFIX + MarshmallowActivity.Item.TFL);

        mChromeImage.setOnClickListener(mOnClickListener);
        mTflImage.setOnClickListener(mOnClickListener);
        mBitcoinImage.setOnClickListener(mOnClickListener);

        return view;
    }

    public void setListener(ImageClickListener listener) {
        mListener = listener;
    }
}
